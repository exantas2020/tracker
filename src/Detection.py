from typing import NamedTuple


class Point3D(NamedTuple):
    X: float
    Y: float
    Z: float


class BoundingRectangle(NamedTuple):
    x: float
    y: float
    w: float
    h: float


class Detection:
    def __init__(self, ts, image_rect: BoundingRectangle, real_world_coordinates: Point3D):
        self.ts = ts
        self.image_rect = image_rect
        self.point3d = real_world_coordinates

