from CentroidTracker import CentroidTracker
# Trigger through iot hub message
message = [] # list[Detection]
# Load previous detections data from db

# turn into a dict of the form [int, List[Detection]]
previous_detections = {}
# init CentroidTracker
tracker = CentroidTracker(previous_detections)

# process new detections
tracking_info = tracker.update(message)

# save to db
