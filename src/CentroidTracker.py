import math
import os
from typing import Dict

from rtree import index

from dotenv import load_dotenv

from Detection import Detection, Point3D

load_dotenv()


class CentroidTracker:

    def __init__(self, tracked_objects: Dict[int, list[Detection]] = {}):
        self.nextObjectID: int = 0
        self.tracked_objects: Dict[int, list[Detection]] = tracked_objects

    def delete(self):
        """ delete the objectID from the tracked interactions """
        self.tracked_objects.clear()

    def register(self, coordinate: Detection):
        """ register a new interaction as an objectID """
        self.tracked_objects[self.nextObjectID] = [coordinate]
        self.nextObjectID += 1

    @staticmethod
    def _get_distance(p1: Point3D, p2: Point3D) -> float:
        x1, y1, z1 = p1
        x2, y2, z2 = p2
        return math.sqrt(math.pow((x1 - x2), 2) + math.pow((y1 - y2), 2) + math.pow((z1 - z2), 2))

    def update(self, detections: list[Detection]) -> Dict[int, Detection]:
        """ check whether incoming centroids is a new object """
        if not self.tracked_objects:
            for detection in detections:
                self.register(detection)
        else:
            tree = index.Index()
            for idx, locations in self.tracked_objects.items():
                tree.insert(idx, locations[-1])

            for detection in detections:
                nearest_id : int = next(tree.nearest(detections, objects=False))
                nearest_location = self.tracked_objects[nearest_id][-1]
                if CentroidTracker.is_close_enough(nearest_location.point3d, detection.point3d):
                    self.tracked_objects[nearest_id].append(detection)
                else:
                    self.register(detection)

        return {idx: locations[-1] for (idx, locations) in self.tracked_objects.items()}

    def get_result(self):
        return self.tracked_objects.copy()

    @staticmethod
    def is_close_enough(last_location: Point3D, rect_location: Point3D):
        return CentroidTracker._get_distance(last_location, rect_location) < float(os.getenv("TRACKING_CHANGE_THRESHOLD"))
